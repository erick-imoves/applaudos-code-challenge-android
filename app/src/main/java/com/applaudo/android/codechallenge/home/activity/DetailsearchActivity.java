package com.applaudo.android.codechallenge.home.activity;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.applaudo.android.codechallenge.R;
import com.applaudo.android.codechallenge.db.AccesData;
import com.applaudo.android.codechallenge.db.Db_datos;
import com.applaudo.android.codechallenge.detail.model.DataEpisodes;
import com.applaudo.android.codechallenge.detail.model.DataGenres;
import com.applaudo.android.codechallenge.detail.model.Episode;
import com.applaudo.android.codechallenge.detail.model.Genres;
import com.applaudo.android.codechallenge.home.model.Datum;
import com.applaudo.android.codechallenge.home.model.Detail;
import com.applaudo.android.codechallenge.rest.ApiClient;
import com.applaudo.android.codechallenge.rest.ApiInterface;
import com.bumptech.glide.Glide;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsearchActivity extends AppCompatActivity {
    ApiInterface apiService;
    ProgressDialog mProgressDialog;

    TextView txtMainTitle,txtCanonicalTitle,txtType,txtYear,txtAverage,txtEpisode,txtAgeRaiting,txtStatus,txtSynopsis,txtYouTube;
    ImageView imgPoster;
    ImageView share;
    ImageView add;
    List<DataEpisodes> dataEpisodesList;
    List<DataGenres> dataGenresList;

    String idSearch;
    LinearLayout linearGenres;
    TextView[] txtGenres;

    LinearLayout linearMain;
    LinearLayout[] linearEpisodes;
    TextView[] txtNumber;
    TextView[] txtEpisodesName;

    AccesData accesData;

    Cursor cursorDetail;

    Datum datum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailsearch);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(R.string.details);

        apiService= ApiClient.getClient().create(ApiInterface.class);
        accesData = new AccesData();
        accesData.Instantiate(this);

        imgPoster=(ImageView)findViewById(R.id.imgPoster);
        txtMainTitle=(TextView)findViewById(R.id.txtMainTitle);
        txtCanonicalTitle=(TextView)findViewById(R.id.txtCanonicalTitle);
        txtType=(TextView)findViewById(R.id.txtType);
        txtYear=(TextView)findViewById(R.id.txtYear);
        txtAverage=(TextView)findViewById(R.id.txtAverage);
        txtEpisode=(TextView)findViewById(R.id.txtEpisode);
        txtAgeRaiting=(TextView)findViewById(R.id.txtAgeRaiting);
        txtStatus=(TextView)findViewById(R.id.txtStatus);
        txtSynopsis=(TextView)findViewById(R.id.txtSynopsis);
        txtYouTube=(TextView)findViewById(R.id.txtYouTube);
        linearMain = (LinearLayout) findViewById(R.id.linearMain);
        linearGenres = (LinearLayout) findViewById(R.id.linearGenres);
        share=(ImageView)findViewById(R.id.share);
        add=(ImageView)findViewById(R.id.add);



        idSearch=getIntent().getExtras().getString("id");
        getDataDatil(idSearch);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Do you want to watch "+datum.getAttributes().getTitles().getEnJp()+" is the better anime: http://www.youtube.com/watch?v="+datum.getAttributes().getYoutubeVideoId());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Db_datos db_datos = new Db_datos("" + datum.getId(),
                        "" + datum.getType(),
                        "AddMy",
                        "" + datum.getAttributes().getCanonicalTitle(),
                        "" + datum.getAttributes().getStartDate(),
                        "" + datum.getAttributes().getAgeRating(),
                        "" + datum.getAttributes().getEpisodeLength(),
                        "" + datum.getAttributes().getAgeRating(),
                        "" + datum.getAttributes().getStatus(),
                        "" + datum.getAttributes().getSynopsis(),
                        "" + datum.getAttributes().getYoutubeVideoId(),
                        "" + datum.getAttributes().getTitles().getEnJp(),
                        "" + datum.getAttributes().getPosterImage().getTiny(),
                        "0");
                Cursor cursor=accesData.getDataById(""+datum.getId());
                if(cursor.getCount()==0) {
                    accesData.dbDatosDao.insert(db_datos);
                    Toast.makeText(DetailsearchActivity.this,"Added",Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(DetailsearchActivity.this,"Already exist",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void getDataDatil(String iD){
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(getResources().getString(R.string.cargando));
        mProgressDialog.show();

        Call<Detail> call = apiService.detail(iD);

        call.enqueue(new Callback<Detail>() {
            @Override
            public void onResponse(Call<Detail> call, Response<Detail> response) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                try {
                    datum = response.body().getData();
                    setDataDetail(datum);
                }catch (Exception e){
                    Log.i("example", "Error, body: " + e.toString());
                }
            }

            @Override
            public void onFailure(Call<Detail> call, Throwable t) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                Log.i("Error", "not conecction with server: " + t.toString());
            }
        });
    }


    public void setDataDetail(final Datum datos){
        Glide.with(this)
                .load(""+datos.getAttributes().getPosterImage().getTiny())
                .into(imgPoster);
        txtMainTitle.setText(""+datos.getAttributes().getTitles().getEnJp());
        txtCanonicalTitle.setText(""+datos.getAttributes().getCanonicalTitle());
        txtType.setText(""+datos.getType());
        txtYear.setText(""+datos.getAttributes().getStartDate());
        txtAverage.setText(""+datos.getAttributes().getAverageRating());
        txtEpisode.setText(""+datos.getAttributes().getEpisodeLength());
        txtAgeRaiting.setText(""+datos.getAttributes().getAgeRating());
        txtStatus.setText(""+datos.getAttributes().getStatus());
        txtSynopsis.setText(""+datos.getAttributes().getSynopsis());

        txtYouTube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linkYouTube(DetailsearchActivity.this,""+datos.getAttributes().getYoutubeVideoId());
            }
        });

        getDataEpisodes(""+datos.getId());
    }

    public void linkYouTube(Context context, String idYoutube){
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:"+idYoutube));
        Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v="+idYoutube));
        try{
            context.startActivity(appIntent);
        }catch (ActivityNotFoundException ex){
            context.startActivity(webIntent);
        }
    }


    public void getDataEpisodes(final String iD){
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(getResources().getString(R.string.cargando));
        mProgressDialog.show();

        Call<Episode> call = apiService.dataEpisode(iD);

        call.enqueue(new Callback<Episode>() {
            @Override
            public void onResponse(Call<Episode> call, Response<Episode> response) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                try {
                    dataEpisodesList = response.body().getData();
                    getListEpisodes(dataEpisodesList);
                }catch (Exception e){
                    Log.i("example", "Error, body: " + e.toString());
                }
                getGenres(iD);
            }

            @Override
            public void onFailure(Call<Episode> call, Throwable t) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                Log.i("Error", "not conecction with server: " + t.toString());
            }
        });

    }


    public void getListEpisodes(final List<DataEpisodes> list){
        linearEpisodes = new LinearLayout[list.size()];
        txtNumber = new TextView[list.size()];
        txtEpisodesName = new TextView[list.size()];

        for (int i = 0; i < list.size(); i++) {
            linearEpisodes[i] = new LinearLayout(this);
            txtNumber[i] = new TextView(this);
            txtEpisodesName[i] =new TextView(this);

            txtNumber[i].setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT,7f));
            txtEpisodesName[i].setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT,1f));

            if(list.get(i).getAttributes().getCanonicalTitle().length()>0) {
                txtNumber[i].setText("-");
                txtEpisodesName[i].setText(list.get(i).getAttributes().getCanonicalTitle());
            }

            linearEpisodes[i].setGravity(Gravity.CENTER|Gravity.CENTER);
            linearEpisodes[i].setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            linearEpisodes[i].addView(txtNumber[i]);
            linearEpisodes[i].addView(txtEpisodesName[i]);
            linearMain.addView(linearEpisodes[i],params);
        }
    }

    public void getGenres(String iD){
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(getResources().getString(R.string.cargando));
        mProgressDialog.show();

        Call<Genres> call = apiService.dateGenres(iD);

        call.enqueue(new Callback<Genres>() {
            @Override
            public void onResponse(Call<Genres> call, Response<Genres> response) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                try {
                    dataGenresList = response.body().getData();
                    getListGenres(dataGenresList);
                }catch (Exception e){
                    Log.i("example", "Error, body: " + e.toString());
                }
            }

            @Override
            public void onFailure(Call<Genres> call, Throwable t) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                Log.i("Error", "not conecction with server: " + t.toString());
            }
        });
    }

    public void getListGenres(final List<DataGenres> list){

        txtGenres = new TextView[list.size()];

        for (int i = 0; i < list.size(); i++) {
            txtGenres[i] =new TextView(this);

            txtGenres[i].setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT,1f));

            txtGenres[i].setText(list.get(i).getAttributesGenres().getName());

            linearGenres.addView(txtGenres[i]);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
