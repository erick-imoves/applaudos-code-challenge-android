package com.applaudo.android.codechallenge;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.applaudo.android.codechallenge.db.AccesData;
import com.applaudo.android.codechallenge.db.Db_datos;
import com.applaudo.android.codechallenge.db.Db_generalGenres;
import com.applaudo.android.codechallenge.detail.model.DataGenres;
import com.applaudo.android.codechallenge.detail.model.Genres;
import com.applaudo.android.codechallenge.home.activity.HomeActivity;
import com.applaudo.android.codechallenge.home.model.Anime;
import com.applaudo.android.codechallenge.home.model.Datum;
import com.applaudo.android.codechallenge.rest.ApiClient;
import com.applaudo.android.codechallenge.rest.ApiInterface;
import com.applaudo.android.codechallenge.util.ConnectedClass;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    ApiInterface apiService;

    List<Datum> listDatos;
    List<DataGenres> dataGenresList;

    AccesData accesData;

    Cursor cursorGenres;

    Button btnOffline;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        apiService= ApiClient.getClient().create(ApiInterface.class);
        accesData = new AccesData();
        accesData.Instantiate(this);

        btnOffline=(Button)findViewById(R.id.btnOffline);

        btnOffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(MainActivity.this, HomeActivity.class);
                startActivity(i);
                finish();
            }
        });

        ConnectedClass c=new ConnectedClass();
        cursorGenres=accesData.getAllGenres();
        if(!c.isConected(this) && cursorGenres.getCount()==0){
            Toast.makeText(MainActivity.this,"No available connections",Toast.LENGTH_LONG).show();
        }else if(!c.isConected(this)){
            Toast.makeText(MainActivity.this,"Offline Mode",Toast.LENGTH_LONG).show();
            Intent i=new Intent(MainActivity.this, HomeActivity.class);
            startActivity(i);
            finish();
        }else if(cursorGenres.getCount()>0){
            getDataPerGenres();
        }else{
            getGenres();
        }
    }

    public void getGenres(){

        Call<Genres> call = apiService.dataGeneralGenres();

        call.enqueue(new Callback<Genres>() {
            @Override
            public void onResponse(Call<Genres> call, Response<Genres> response) {
                try {
                    dataGenresList=response.body().getData();
                    for (int x = 0; x < dataGenresList.size(); x++) {
                        Db_generalGenres db_datos = new Db_generalGenres(""+dataGenresList.get(x).getId(),
                                ""+dataGenresList.get(x).getType(),
                                ""+dataGenresList.get(x).getAttributesGenres().getName());
                        accesData.db_generalGenresDao.insert(db_datos);
                    }
                    getDataPerGenres();
                }catch (Exception e){
                    Log.i("example", "Error, body: " + e.toString());
                }
            }

            @Override
            public void onFailure(Call<Genres> call, Throwable t) {
                Log.i("Error", "not conecction with server: " + t.toString());
            }
        });
    }

    public void getTrendingData(){

        Call<Anime> call = apiService.trendingAnime();

        call.enqueue(new Callback<Anime>() {
            @Override
            public void onResponse(Call<Anime> call, Response<Anime> response) {
                try {
                    listDatos = response.body().getData();
                    for (int x = 0; x < listDatos.size(); x++) {
                        Db_datos db_datos = new Db_datos("" + listDatos.get(x).getId(),
                                "" + listDatos.get(x).getType(),
                                "trending",
                                "" + listDatos.get(x).getAttributes().getCanonicalTitle(),
                                "" + listDatos.get(x).getAttributes().getStartDate(),
                                "" + listDatos.get(x).getAttributes().getAgeRating(),
                                "" + listDatos.get(x).getAttributes().getEpisodeLength(),
                                "" + listDatos.get(x).getAttributes().getAgeRating(),
                                "" + listDatos.get(x).getAttributes().getStatus(),
                                "" + listDatos.get(x).getAttributes().getSynopsis(),
                                "" + listDatos.get(x).getAttributes().getYoutubeVideoId(),
                                "" + listDatos.get(x).getAttributes().getTitles().getEnJp(),
                                "" + listDatos.get(x).getAttributes().getPosterImage().getTiny(),
                                "0");
                        Cursor cursor=accesData.getDataById(""+listDatos.get(x).getId());
                        if(cursor.getCount()==0) {
                            accesData.dbDatosDao.insert(db_datos);
                        }
                    }
                    Intent i=new Intent(MainActivity.this, HomeActivity.class);
                    startActivity(i);
                    finish();

                }catch (Exception e){
                    Log.i("example", "Error, body: " + e.toString());
                }

            }

            @Override
            public void onFailure(Call<Anime> call, Throwable t) {
                Toast.makeText(MainActivity.this,getResources().getString(R.string.Error4),Toast.LENGTH_LONG).show();
                Log.i("Error", "not conecction with server: " + t.toString());
                btnOffline.setVisibility(View.VISIBLE);
            }
        });
    }

    public void getDataPerGenres(){

        int n=1;
        cursorGenres=accesData.getAllGenres();
        for(int x=0; x<cursorGenres.getCount(); x++){
            cursorGenres.moveToPosition(x);
            getAllData(cursorGenres.getString(2));
            n++;
            if(n==cursorGenres.getCount()){
                getTrendingData();
            }

        }


    }

    public void getAllData(final String nameGenre){
        Call<Anime> call = apiService.dateFilterGenres(""+nameGenre);

        call.enqueue(new Callback<Anime>() {
            @Override
            public void onResponse(Call<Anime> call, Response<Anime> response) {
                try {
                    listDatos = response.body().getData();
                    for (int x = 0; x < listDatos.size(); x++) {
                        Db_datos db_datos = new Db_datos("" + listDatos.get(x).getId(),
                                "" + listDatos.get(x).getType(),
                                ""+nameGenre,
                                "" + listDatos.get(x).getAttributes().getCanonicalTitle(),
                                "" + listDatos.get(x).getAttributes().getStartDate(),
                                "" + listDatos.get(x).getAttributes().getAgeRating(),
                                "" + listDatos.get(x).getAttributes().getEpisodeLength(),
                                "" + listDatos.get(x).getAttributes().getAgeRating(),
                                "" + listDatos.get(x).getAttributes().getStatus(),
                                "" + listDatos.get(x).getAttributes().getSynopsis(),
                                "" + listDatos.get(x).getAttributes().getYoutubeVideoId(),
                                "" + listDatos.get(x).getAttributes().getTitles().getEnJp(),
                                "" + listDatos.get(x).getAttributes().getPosterImage().getTiny(),
                                "0");
                        Cursor cursor=accesData.getDataById(""+listDatos.get(x).getId());
                        if(cursor.getCount()==0) {
                            accesData.dbDatosDao.insert(db_datos);
                        }
                    }
                }catch (Exception e){
                    Log.i("example", "Error, body: " + e.toString());
                }

            }

            @Override
            public void onFailure(Call<Anime> call, Throwable t) {

                Log.i("Error", "not conecction with server: " + t.toString());
            }
        });

    }

}
