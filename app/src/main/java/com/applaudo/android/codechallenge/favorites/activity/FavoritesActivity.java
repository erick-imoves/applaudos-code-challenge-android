package com.applaudo.android.codechallenge.favorites.activity;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.applaudo.android.codechallenge.R;
import com.applaudo.android.codechallenge.db.AccesData;
import com.applaudo.android.codechallenge.favorites.adapter.FavoritesAdapter;

public class FavoritesActivity extends AppCompatActivity {

    Cursor cursorFavorites;
    AccesData accesData;

    RecyclerView recyclerFavorites;
    FavoritesAdapter favoritesAdapter;
    SwipeRefreshLayout swiperefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(R.string.favoritesTitle);

        accesData = new AccesData();
        accesData.Instantiate(this);

        recyclerFavorites=(RecyclerView)findViewById(R.id.recyclerFavorites);
        swiperefresh=(SwipeRefreshLayout)findViewById(R.id.swiperefresh);

        getFavorites();

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getFavorites();
                swiperefresh.setRefreshing(false);
            }
        });

    }

    public void getFavorites(){
        cursorFavorites=accesData.getDataByFavorites("1");
        favoritesAdapter=new FavoritesAdapter(cursorFavorites,FavoritesActivity.this,recyclerFavorites);
        LinearLayoutManager layoutManager = new LinearLayoutManager(FavoritesActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerFavorites.setLayoutManager(layoutManager);
        recyclerFavorites.setItemAnimator(new DefaultItemAnimator());
        recyclerFavorites.setAdapter(favoritesAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
