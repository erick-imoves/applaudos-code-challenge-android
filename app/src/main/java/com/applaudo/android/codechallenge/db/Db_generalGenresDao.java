package com.applaudo.android.codechallenge.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

/**
 * Created by erick on 16/07/2018.
 */

public class Db_generalGenresDao extends AbstractDao<Db_generalGenres, Void> {
    public static final String TABLENAME = "DB_GENERALGENRES";


    public static class Properties {
        public final static Property id = new Property(0, String.class, "id", false, "ID");
        public final static Property type = new Property(1, String.class, "type", false, "TYPE");
        public final static Property name = new Property(2, String.class, "name", false, "NAME");

    };


    public Db_generalGenresDao(DaoConfig config) {
        super(config);
    }

    public Db_generalGenresDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"DB_GENERALGENRES\" (" + //
                "\"ID\" TEXT," + // 0:
                "\"TYPE\" TEXT," + // 1:
                "\"NAME\" TEXT);"); // 2:
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"DB_GENERALGENRES\"";
        db.execSQL(sql);
    }

    @Override
    protected void bindValues(SQLiteStatement stmt, Db_generalGenres entity) {
        stmt.clearBindings();

        String id = entity.getId();
        if (id != null) {
            stmt.bindString(1, id);
        }

        String type = entity.getType();
        if (type != null) {
            stmt.bindString(2, type);
        }

        String name = entity.getName();
        if (name != null) {
            stmt.bindString(3, name);
        }

    }

    @Override
    public Void readKey(Cursor cursor, int offset) {
        return null;
    }

    @Override
    public Db_generalGenres readEntity(Cursor cursor, int offset) {
        Db_generalGenres entity = new Db_generalGenres( //
                cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0),
                cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1),
                cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2)
        );
        return entity;
    }

    @Override
    public void readEntity(Cursor cursor, Db_generalGenres entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0));
        entity.setType(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setName(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));

    }

    @Override
    protected Void updateKeyAfterInsert(Db_generalGenres entity, long rowId) {
        return null;
    }

    @Override
    public Void getKey(Db_generalGenres entity) {
        return null;
    }

    @Override
    protected boolean isEntityUpdateable()   {
        return true;
    }
}
