package com.applaudo.android.codechallenge.detail.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by erick on 15/07/2018.
 */

public class DataGenres {
    @SerializedName("id")
    String id;
    @SerializedName("type")
    String type;
    @SerializedName("attributes")
    AttributesGenres attributesGenres;

    public DataGenres(String id, String type, AttributesGenres attributesGenres) {
        this.id = id;
        this.type = type;
        this.attributesGenres = attributesGenres;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public AttributesGenres getAttributesGenres() {
        return attributesGenres;
    }

    public void setAttributesGenres(AttributesGenres attributesGenres) {
        this.attributesGenres = attributesGenres;
    }
}
