package com.applaudo.android.codechallenge.home.model;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by erick on 14/07/2018.
 */

public class Anime {
    @SerializedName("data")
    List<Datum> data = null;

    public Anime(List<Datum> data) {
        this.data = data;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
}
