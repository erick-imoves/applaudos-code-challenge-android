package com.applaudo.android.codechallenge.rest;

import com.applaudo.android.codechallenge.detail.model.Episode;
import com.applaudo.android.codechallenge.detail.model.Genres;
import com.applaudo.android.codechallenge.home.model.Anime;
import com.applaudo.android.codechallenge.home.model.Detail;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by erick on 14/07/2018.
 */

public interface ApiInterface {

    @GET("trending/anime")
    @Headers("Content-Type: application/vnd.api+json")
    Call<Anime> trendingAnime();

    @GET("genres")
    @Headers("Content-Type: application/vnd.api+json")
    Call<Genres> dataGeneralGenres();

    @GET("anime/{id}")
    @Headers("Content-Type: application/vnd.api+json")
    Call<Detail> detail(@Path("id") String id);

    @GET("anime/{id}/episodes")
    @Headers("Content-Type: application/vnd.api+json")
    Call<Episode> dataEpisode(@Path("id") String id);

    @GET("anime/{id}/genres")
    @Headers("Content-Type: application/vnd.api+json")
    Call<Genres> dateGenres(@Path("id") String id);

    @GET("anime")
    @Headers("Content-Type: application/vnd.api+json")
    Call<Anime> dateFilter(@Query("filter[text]") String id);

    @GET("anime")
    @Headers("Content-Type: application/vnd.api+json")
    Call<Anime> dateFilterGenres(@Query("filter[genres]") String id);
}
