package com.applaudo.android.codechallenge.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by erick on 17/07/2018.
 */

public class ConnectedClass {
    public ConnectedClass() {
    }
    //IS CONECTED
    public boolean isConected(Context context){
        boolean conected = false;
        ConnectivityManager connec=(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connec != null;
        NetworkInfo[] nets = connec.getAllNetworkInfo();
        for(int i=0; i<nets.length; i++){
            if(nets[i].getState()==NetworkInfo.State.CONNECTED){
                conected=true;
            }
        }
        return conected;
    }
}
