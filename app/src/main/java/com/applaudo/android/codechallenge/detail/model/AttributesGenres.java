package com.applaudo.android.codechallenge.detail.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by erick on 15/07/2018.
 */

public class AttributesGenres {
    @SerializedName("name")
    String name;

    public AttributesGenres(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
