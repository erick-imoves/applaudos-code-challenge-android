package com.applaudo.android.codechallenge.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

/**
 * Created by erick on 16/07/2018.
 */

public class Db_datosDao  extends AbstractDao<Db_datos, Void> {

    public static final String TABLENAME = "DB_DATOS";

    public static class Properties {
        public final static Property idDatos = new Property(0, String.class, "idDatos", false, "IDDATOS");
        public final static Property type = new Property(1, String.class, "type", false, "TYPE");
        public final static Property genre = new Property(2, String.class, "genre", false, "GENRE");
        public final static Property canonicalTitle = new Property(3, String.class, "canonicalTitle", false, "CANONICALTITLE");
        public final static Property startDate = new Property(4, String.class, "startDate", false, "STARTDATE");
        public final static Property averageRating = new Property(5, String.class, "averageRating", false, "AVERAGERATING");
        public final static Property episodeLength = new Property(6, String.class, "episodeLength", false, "EPISODELENGTH");
        public final static Property ageRating = new Property(7, String.class, "ageRating", false, "AGERATING");
        public final static Property status = new Property(8, String.class, "status", false, "STATUS");
        public final static Property synopsis = new Property(9, String.class, "synopsis", false, "SYNOPSIS");
        public final static Property youtubeVideoId = new Property(10, String.class, "youtubeVideoId", false, "YOUTUBEVIDEOID");
        public final static Property en_jp = new Property(11, String.class, "en_jp", false, "EN_JP");
        public final static Property tiny = new Property(12, String.class, "tiny", false, "TINY");
        public final static Property favorites = new Property(13, String.class, "favorites", false, "FAVORITES");
    };


    public Db_datosDao(DaoConfig config) {
        super(config);
    }

    public Db_datosDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"DB_DATOS\" (" +
                "\"IDDATOS\" TEXT," + // 0
                "\"TYPE\" TEXT," + // 1
                "\"GENRE\" TEXT," + // 2
                "\"CANONICALTITLE\" TEXT," + // 3
                "\"STARTDATE\" TEXT," + // 4
                "\"AVERAGERATING\" TEXT," + // 5
                "\"EPISODELENGTH\" TEXT," + // 6
                "\"AGERATING\" TEXT," + // 7
                "\"STATUS\" TEXT," + // 8
                "\"SYNOPSIS\" TEXT," + // 9
                "\"YOUTUBEVIDEOID\" TEXT," + // 10
                "\"EN_JP\" TEXT," + // 11
                "\"TINY\" TEXT," + // 12
                "\"FAVORITES\" TEXT);"); // 13
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"DB_DATOS\"";
        db.execSQL(sql);
    }


    @Override
    protected void bindValues(SQLiteStatement stmt, Db_datos entity) {
        stmt.clearBindings();

        String idDatos = entity.getIdDatos();
        if (idDatos != null) {
            stmt.bindString(1, idDatos);
        }

        String type = entity.getType();
        if (type != null) {
            stmt.bindString(2, type);
        }

        String genre = entity.getGenre();
        if (genre != null) {
            stmt.bindString(3, genre);
        }

        String canonicalTitle = entity.getCanonicalTitle();
        if (canonicalTitle != null) {
            stmt.bindString(4, canonicalTitle);
        }

        String startDate = entity.getStartDate();
        if (startDate != null) {
            stmt.bindString(5, startDate);
        }

        String averageRating = entity.getAverageRating();
        if (averageRating != null) {
            stmt.bindString(6, averageRating);
        }

        String episodeLength = entity.getEpisodeLength();
        if (episodeLength != null) {
            stmt.bindString(7, episodeLength);
        }

        String ageRating = entity.getAgeRating();
        if (ageRating != null) {
            stmt.bindString(8, ageRating);
        }

        String status = entity.getStatus();
        if (status != null) {
            stmt.bindString(9, status);
        }

        String synopsis = entity.getSynopsis();
        if (synopsis != null) {
            stmt.bindString(10, synopsis);
        }

        String youtubeVideoId = entity.getYoutubeVideoId();
        if (youtubeVideoId != null) {
            stmt.bindString(11, youtubeVideoId);
        }

        String en_jp = entity.getEn_jp();
        if (en_jp != null) {
            stmt.bindString(12, en_jp);
        }

        String tiny = entity.getTiny();
        if (tiny != null) {
            stmt.bindString(13, tiny);
        }

        String favorites = entity.getFavorites();
        if (favorites != null) {
            stmt.bindString(14, favorites);
        }
    }


    @Override
    public Void readKey(Cursor cursor, int offset) {
        return null;
    }


    @Override
    public Db_datos readEntity(Cursor cursor, int offset) {
        Db_datos entity = new Db_datos( //
                cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0), //
                cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), //
                cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), //
                cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), //
                cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), //
                cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), //
                cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6), //
                cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7), //
                cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8), //
                cursor.isNull(offset + 9) ? null : cursor.getString(offset + 9), //
                cursor.isNull(offset + 10) ? null : cursor.getString(offset + 10), //
                cursor.isNull(offset + 11) ? null : cursor.getString(offset + 11), //
                cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12), //
                cursor.isNull(offset + 13) ? null : cursor.getString(offset + 13) //
        );
        return entity;
    }

    @Override
    public void readEntity(Cursor cursor, Db_datos entity, int offset) {
        entity.setIdDatos(cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0));
        entity.setType(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setGenre(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setGenre(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setGenre(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setGenre(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setGenre(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
        entity.setGenre(cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7));
        entity.setGenre(cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8));
        entity.setGenre(cursor.isNull(offset + 9) ? null : cursor.getString(offset + 9));
        entity.setGenre(cursor.isNull(offset + 10) ? null : cursor.getString(offset + 10));
        entity.setGenre(cursor.isNull(offset + 11) ? null : cursor.getString(offset + 11));
        entity.setGenre(cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12));
        entity.setGenre(cursor.isNull(offset + 13) ? null : cursor.getString(offset + 12));


    }

    @Override
    protected Void updateKeyAfterInsert(Db_datos entity, long rowId) {
        return null;
    }


    @Override
    public Void getKey(Db_datos entity) {
        return null;
    }


    @Override
    protected boolean isEntityUpdateable()   {
        return true;
    }
}
