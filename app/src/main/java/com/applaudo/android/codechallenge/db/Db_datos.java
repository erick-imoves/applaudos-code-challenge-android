package com.applaudo.android.codechallenge.db;

/**
 * Created by erick on 16/07/2018.
 */

public class Db_datos {
    String idDatos;
    String type;
    String genre;
    String canonicalTitle;
    String startDate;
    String averageRating;
    String episodeLength;
    String ageRating;
    String status;
    String synopsis;
    String youtubeVideoId;
    String en_jp;
    String tiny;
    String favorites;

    public Db_datos(String idDatos, String type, String genre, String canonicalTitle, String startDate, String averageRating, String episodeLength, String ageRating, String status, String synopsis, String youtubeVideoId, String en_jp, String tiny, String favorites) {
        this.idDatos = idDatos;
        this.type = type;
        this.genre = genre;
        this.canonicalTitle = canonicalTitle;
        this.startDate = startDate;
        this.averageRating = averageRating;
        this.episodeLength = episodeLength;
        this.ageRating = ageRating;
        this.status = status;
        this.synopsis = synopsis;
        this.youtubeVideoId = youtubeVideoId;
        this.en_jp = en_jp;
        this.tiny = tiny;
        this.favorites = favorites;
    }

    public String getIdDatos() {
        return idDatos;
    }

    public void setIdDatos(String idDatos) {
        this.idDatos = idDatos;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getCanonicalTitle() {
        return canonicalTitle;
    }

    public void setCanonicalTitle(String canonicalTitle) {
        this.canonicalTitle = canonicalTitle;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(String averageRating) {
        this.averageRating = averageRating;
    }

    public String getEpisodeLength() {
        return episodeLength;
    }

    public void setEpisodeLength(String episodeLength) {
        this.episodeLength = episodeLength;
    }

    public String getAgeRating() {
        return ageRating;
    }

    public void setAgeRating(String ageRating) {
        this.ageRating = ageRating;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getYoutubeVideoId() {
        return youtubeVideoId;
    }

    public void setYoutubeVideoId(String youtubeVideoId) {
        this.youtubeVideoId = youtubeVideoId;
    }

    public String getEn_jp() {
        return en_jp;
    }

    public void setEn_jp(String en_jp) {
        this.en_jp = en_jp;
    }

    public String getTiny() {
        return tiny;
    }

    public void setTiny(String tiny) {
        this.tiny = tiny;
    }

    public String getFavorites() {
        return favorites;
    }

    public void setFavorites(String favorites) {
        this.favorites = favorites;
    }
}
