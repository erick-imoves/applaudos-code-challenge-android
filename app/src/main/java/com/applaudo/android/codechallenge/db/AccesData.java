package com.applaudo.android.codechallenge.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by erick on 16/07/2018.
 */

public class AccesData {
    private SQLiteDatabase db;
    private DaoMaster daoMaster;
    private DaoSession daoSession;
    public Db_datosDao dbDatosDao;
    public Db_generalGenresDao db_generalGenresDao;
    Cursor cursor;

    public void Instantiate(Context context) {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, "com.applaudo.android.codechallenge.db", null);
        db = helper.getWritableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
        dbDatosDao = daoSession.getdbDatosDao();
        db_generalGenresDao = daoSession.getDbGeneralGenresDao();
    }

    public Cursor getData(String genero){
        cursor = db.query(dbDatosDao.getTablename(), dbDatosDao.getAllColumns(), "GENRE LIKE '%"+genero+"%'", null, null, null, null);
        cursor.moveToFirst();
        return cursor;
    }

    public Cursor getAllGenres(){
        cursor = db.query(db_generalGenresDao.getTablename(), db_generalGenresDao.getAllColumns(), null, null, null, null, null);
        cursor.moveToFirst();
        return cursor;
    }

    public Cursor getDataById(String id){
        cursor = db.query(dbDatosDao.getTablename(), dbDatosDao.getAllColumns(), "IDDATOS LIKE '%"+id+"%'", null, null, null, null);
        cursor.moveToFirst();
        return cursor;
    }

    public Cursor getDataByIdAndGenre(String id, String genre){
        cursor = db.query(dbDatosDao.getTablename(), dbDatosDao.getAllColumns(), "IDDATOS LIKE '%"+id+"%' and GENRE LIKE '%"+genre+"%'", null, null, null, null);
        cursor.moveToFirst();
        return cursor;
    }


    public void updateData(String id,String favorites){
        ContentValues valor = new ContentValues();
        valor.put("FAVORITES",    favorites);
        db.update(dbDatosDao.getTablename(), valor, "IDDATOS LIKE '%"+id+"%'", null);
    }

    public Cursor getDataByFavorites(String favorites){
        cursor = db.query(dbDatosDao.getTablename(), dbDatosDao.getAllColumns(), "FAVORITES LIKE '%"+favorites+"%'", null, null, null, null);
        cursor.moveToFirst();
        return cursor;
    }

    public Cursor deleteDatos(String genre){
        cursor = db.rawQuery("DELETE FROM " + dbDatosDao.getTablename() + " where GENRE LIKE '%"+genre+"%'", null);
        cursor.moveToFirst();
        return cursor;
    }
}
