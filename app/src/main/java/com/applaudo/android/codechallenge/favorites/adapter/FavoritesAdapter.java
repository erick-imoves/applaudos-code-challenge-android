package com.applaudo.android.codechallenge.favorites.adapter;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applaudo.android.codechallenge.R;
import com.applaudo.android.codechallenge.detail.activity.DetailActivity;
import com.bumptech.glide.Glide;

/**
 * Created by erick on 17/07/2018.
 */

public class FavoritesAdapter extends  RecyclerView.Adapter<FavoritesAdapter.MyViewHolder>{

    Cursor cursor;
    Activity activity;
    RecyclerView recycler_view;

    public FavoritesAdapter(Cursor cursor, Activity activity, RecyclerView recycler_view) {
        this.cursor = cursor;
        this.activity = activity;
        this.recycler_view = recycler_view;
    }

    @Override
    public FavoritesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_search, parent, false);
        return new FavoritesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FavoritesAdapter.MyViewHolder holder, final int position) {
        cursor.moveToPosition(position);
        holder.txtTitulo.setText(""+cursor.getString(11));
        holder.txtDescrip.setText(""+cursor.getString(9));
        Glide.with(activity)
                .load(cursor.getString(12))
                .into(holder.imgPoster);
        holder.linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cursor.moveToPosition(position);
                Intent i=new Intent(activity, DetailActivity.class);
                i.putExtra("id",""+cursor.getString(0));
                activity.startActivity(i);
                activity.overridePendingTransition(R.anim.zoom_forward_in,R.anim.zoom_forward_out);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cursor.getCount();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linear;
        TextView txtTitulo;
        TextView txtDescrip;
        ImageView imgPoster;
        public MyViewHolder(View itemView) {
            super(itemView);
            linear=itemView.findViewById(R.id.linear);
            txtTitulo=itemView.findViewById(R.id.txtTitulo);
            txtDescrip=itemView.findViewById(R.id.txtDescrip);
            imgPoster=itemView.findViewById(R.id.imgPoster);
        }
    }
}
