package com.applaudo.android.codechallenge.home.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by erick on 14/07/2018.
 */

public class Attributes {
    @SerializedName("createdAt")
    String createdAt;
    @SerializedName("updatedAt")
    String updatedAt;
    @SerializedName("slug")
    String slug;
    @SerializedName("synopsis")
    String synopsis;
    @SerializedName("coverImageTopOffset")
    int coverImageTopOffset;

    @SerializedName("titles")
    Titles titles;

    @SerializedName("canonicalTitle")
    String canonicalTitle;
    @SerializedName("abbreviatedTitles")
    List<String> abbreviatedTitles = null;
    @SerializedName("averageRating")
    String averageRating;

    //@SerializedName("ratingFrequencies")
    //private RatingFrequencies ratingFrequencies;

    @SerializedName("userCount")
    int userCount;
    @SerializedName("favoritesCount")
    int favoritesCount;
    @SerializedName("startDate")
    String startDate;
    @SerializedName("endDate")
    String endDate;

   // @SerializedName("nextRelease")
   // private Object nextRelease;

    @SerializedName("popularityRank")
    int popularityRank;
    @SerializedName("ratingRank")
    int ratingRank;
    @SerializedName("ageRating")
    String ageRating;
    @SerializedName("ageRatingGuide")
    String ageRatingGuide;
    @SerializedName("subtype")
    String subtype;
    @SerializedName("status")
    String status;
    @SerializedName("tba")
    String tba;

    @SerializedName("posterImage")
    PosterImage posterImage;
    @SerializedName("coverImage")
    CoverImage coverImage;


    @SerializedName("episodeCount")
    int episodeCount;
    @SerializedName("episodeLength")
    int episodeLength;
    @SerializedName("youtubeVideoId")
    String youtubeVideoId;
    @SerializedName("showType")
    String showType;
    @SerializedName("nsfw")
    Boolean nsfw;

    public Attributes(String createdAt, String updatedAt, String slug, String synopsis, int coverImageTopOffset, Titles titles, String canonicalTitle, List<String> abbreviatedTitles, String averageRating, int userCount, int favoritesCount, String startDate, String endDate, int popularityRank, int ratingRank, String ageRating, String ageRatingGuide, String subtype, String status, String tba, PosterImage posterImage, CoverImage coverImage, int episodeCount, int episodeLength, String youtubeVideoId, String showType, Boolean nsfw) {
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.slug = slug;
        this.synopsis = synopsis;
        this.coverImageTopOffset = coverImageTopOffset;
        this.titles = titles;
        this.canonicalTitle = canonicalTitle;
        this.abbreviatedTitles = abbreviatedTitles;
        this.averageRating = averageRating;
        this.userCount = userCount;
        this.favoritesCount = favoritesCount;
        this.startDate = startDate;
        this.endDate = endDate;
        this.popularityRank = popularityRank;
        this.ratingRank = ratingRank;
        this.ageRating = ageRating;
        this.ageRatingGuide = ageRatingGuide;
        this.subtype = subtype;
        this.status = status;
        this.tba = tba;
        this.posterImage = posterImage;
        this.coverImage = coverImage;
        this.episodeCount = episodeCount;
        this.episodeLength = episodeLength;
        this.youtubeVideoId = youtubeVideoId;
        this.showType = showType;
        this.nsfw = nsfw;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public int getCoverImageTopOffset() {
        return coverImageTopOffset;
    }

    public void setCoverImageTopOffset(int coverImageTopOffset) {
        this.coverImageTopOffset = coverImageTopOffset;
    }

    public Titles getTitles() {
        return titles;
    }

    public void setTitles(Titles titles) {
        this.titles = titles;
    }

    public String getCanonicalTitle() {
        return canonicalTitle;
    }

    public void setCanonicalTitle(String canonicalTitle) {
        this.canonicalTitle = canonicalTitle;
    }

    public List<String> getAbbreviatedTitles() {
        return abbreviatedTitles;
    }

    public void setAbbreviatedTitles(List<String> abbreviatedTitles) {
        this.abbreviatedTitles = abbreviatedTitles;
    }

    public String getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(String averageRating) {
        this.averageRating = averageRating;
    }

    public int getUserCount() {
        return userCount;
    }

    public void setUserCount(int userCount) {
        this.userCount = userCount;
    }

    public int getFavoritesCount() {
        return favoritesCount;
    }

    public void setFavoritesCount(int favoritesCount) {
        this.favoritesCount = favoritesCount;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getPopularityRank() {
        return popularityRank;
    }

    public void setPopularityRank(int popularityRank) {
        this.popularityRank = popularityRank;
    }

    public int getRatingRank() {
        return ratingRank;
    }

    public void setRatingRank(int ratingRank) {
        this.ratingRank = ratingRank;
    }

    public String getAgeRating() {
        return ageRating;
    }

    public void setAgeRating(String ageRating) {
        this.ageRating = ageRating;
    }

    public String getAgeRatingGuide() {
        return ageRatingGuide;
    }

    public void setAgeRatingGuide(String ageRatingGuide) {
        this.ageRatingGuide = ageRatingGuide;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTba() {
        return tba;
    }

    public void setTba(String tba) {
        this.tba = tba;
    }

    public PosterImage getPosterImage() {
        return posterImage;
    }

    public void setPosterImage(PosterImage posterImage) {
        this.posterImage = posterImage;
    }

    public CoverImage getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(CoverImage coverImage) {
        this.coverImage = coverImage;
    }

    public int getEpisodeCount() {
        return episodeCount;
    }

    public void setEpisodeCount(int episodeCount) {
        this.episodeCount = episodeCount;
    }

    public int getEpisodeLength() {
        return episodeLength;
    }

    public void setEpisodeLength(int episodeLength) {
        this.episodeLength = episodeLength;
    }

    public String getYoutubeVideoId() {
        return youtubeVideoId;
    }

    public void setYoutubeVideoId(String youtubeVideoId) {
        this.youtubeVideoId = youtubeVideoId;
    }

    public String getShowType() {
        return showType;
    }

    public void setShowType(String showType) {
        this.showType = showType;
    }

    public Boolean getNsfw() {
        return nsfw;
    }

    public void setNsfw(Boolean nsfw) {
        this.nsfw = nsfw;
    }
}
