package com.applaudo.android.codechallenge.detail.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by erick on 15/07/2018.
 */

public class Genres {
    @SerializedName("data")
    List<DataGenres> data = null;

    public Genres(List<DataGenres> data) {
        this.data = data;
    }

    public List<DataGenres> getData() {
        return data;
    }

    public void setData(List<DataGenres> data) {
        this.data = data;
    }
}
