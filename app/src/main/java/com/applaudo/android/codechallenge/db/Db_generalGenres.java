package com.applaudo.android.codechallenge.db;

/**
 * Created by erick on 16/07/2018.
 */

public class Db_generalGenres {
    String id;
    String type;
    String name;

    public Db_generalGenres(String id, String type, String name) {
        this.id = id;
        this.type = type;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
