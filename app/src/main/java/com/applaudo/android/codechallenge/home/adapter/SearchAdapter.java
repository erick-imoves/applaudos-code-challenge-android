package com.applaudo.android.codechallenge.home.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applaudo.android.codechallenge.R;
import com.applaudo.android.codechallenge.home.activity.DetailsearchActivity;
import com.applaudo.android.codechallenge.home.model.Datum;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by erick on 15/07/2018.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder> {
    List<Datum> listDatos;
    Activity activity;
    RecyclerView recycler_view;

    public SearchAdapter(List<Datum> listDatos, Activity activity, RecyclerView recycler_view) {
        this.listDatos = listDatos;
        this.activity = activity;
        this.recycler_view = recycler_view;
    }

    @Override
    public SearchAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_search, parent, false);
        return new SearchAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SearchAdapter.MyViewHolder holder, final int position) {
        holder.txtTitulo.setText(""+listDatos.get(position).getAttributes().getTitles().getEnJp());
        holder.txtDescrip.setText(""+listDatos.get(position).getAttributes().getSynopsis());
        Glide.with(activity)
                .load(""+listDatos.get(position).getAttributes().getPosterImage().getTiny())
                .into(holder.imgPoster);
        holder.linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(activity, DetailsearchActivity.class);
                i.putExtra("id",""+listDatos.get(position).getId());
                activity.startActivity(i);
                activity.overridePendingTransition(R.anim.zoom_forward_in,R.anim.zoom_forward_out);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listDatos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linear;
        TextView txtTitulo;
        TextView txtDescrip;
        ImageView imgPoster;
        public MyViewHolder(View itemView) {
            super(itemView);
            linear=itemView.findViewById(R.id.linear);
            txtTitulo=itemView.findViewById(R.id.txtTitulo);
            txtDescrip=itemView.findViewById(R.id.txtDescrip);
            imgPoster=itemView.findViewById(R.id.imgPoster);
        }
    }
}
