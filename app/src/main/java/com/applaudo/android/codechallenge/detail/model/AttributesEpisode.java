package com.applaudo.android.codechallenge.detail.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by erick on 15/07/2018.
 */

public class AttributesEpisode {
    @SerializedName("number")
    int number;
    @SerializedName("canonicalTitle")
    String canonicalTitle;
    @SerializedName("thumbnail")
    Thumbnail thumbnail;

    public AttributesEpisode(int number, String canonicalTitle, Thumbnail thumbnail) {
        this.number = number;
        this.canonicalTitle = canonicalTitle;
        this.thumbnail = thumbnail;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getCanonicalTitle() {
        return canonicalTitle;
    }

    public void setCanonicalTitle(String canonicalTitle) {
        this.canonicalTitle = canonicalTitle;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }
}
