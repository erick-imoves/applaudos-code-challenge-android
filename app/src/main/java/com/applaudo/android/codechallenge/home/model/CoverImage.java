package com.applaudo.android.codechallenge.home.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by erick on 14/07/2018.
 */

public class CoverImage {
    @SerializedName("tiny")
    String tiny;
    @SerializedName("small")
    String small;
    @SerializedName("large")
    String large;
    @SerializedName("original")
    String original;

    public CoverImage(String tiny, String small, String large, String original) {
        this.tiny = tiny;
        this.small = small;
        this.large = large;
        this.original = original;
    }

    public String getTiny() {
        return tiny;
    }

    public void setTiny(String tiny) {
        this.tiny = tiny;
    }

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }
}
