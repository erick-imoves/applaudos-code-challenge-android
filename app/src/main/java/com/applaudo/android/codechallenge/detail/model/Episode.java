package com.applaudo.android.codechallenge.detail.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by erick on 15/07/2018.
 */

public class Episode {
    @SerializedName("data")
    List<DataEpisodes> data = null;

    public Episode(List<DataEpisodes> data) {
        this.data = data;
    }

    public List<DataEpisodes> getData() {
        return data;
    }

    public void setData(List<DataEpisodes> data) {
        this.data = data;
    }
}
