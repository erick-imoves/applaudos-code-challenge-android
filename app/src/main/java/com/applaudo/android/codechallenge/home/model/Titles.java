package com.applaudo.android.codechallenge.home.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by erick on 14/07/2018.
 */

public class Titles {
    @SerializedName("en")
    String en;
    @SerializedName("en_jp")
    String enJp;
    @SerializedName("ja_jp")
    String jaJp;

    public Titles(String en, String enJp, String jaJp) {
        this.en = en;
        this.enJp = enJp;
        this.jaJp = jaJp;
    }

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }

    public String getEnJp() {
        return enJp;
    }

    public void setEnJp(String enJp) {
        this.enJp = enJp;
    }

    public String getJaJp() {
        return jaJp;
    }

    public void setJaJp(String jaJp) {
        this.jaJp = jaJp;
    }
}
