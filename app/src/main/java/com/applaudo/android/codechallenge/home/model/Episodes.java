package com.applaudo.android.codechallenge.home.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by erick on 14/07/2018.
 */

public class Episodes {
    @SerializedName("links")
    Links links;

    public Episodes(Links links) {
        this.links = links;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

}
