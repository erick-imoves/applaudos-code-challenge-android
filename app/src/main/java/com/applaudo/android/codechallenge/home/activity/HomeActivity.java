package com.applaudo.android.codechallenge.home.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.applaudo.android.codechallenge.R;
import com.applaudo.android.codechallenge.db.AccesData;
import com.applaudo.android.codechallenge.favorites.activity.FavoritesActivity;
import com.applaudo.android.codechallenge.home.adapter.AnimeHorizontalAdapter;
import com.melnykov.fab.FloatingActionButton;

public class HomeActivity extends AppCompatActivity {

    Cursor cursorGenres;
    AccesData accesData;

    AnimeHorizontalAdapter animeHorizontalAdapter;

    TextView[] txtGenres;
    RecyclerView[] recyclerViews;
    LinearLayout linearMain;
    ImageView favorites;
    SwipeRefreshLayout swiperefresh;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        accesData = new AccesData();
        accesData.Instantiate(this);

        favorites=(ImageView)findViewById(R.id.favorites);
        linearMain=(LinearLayout)findViewById(R.id.linearMain);
        swiperefresh=(SwipeRefreshLayout)findViewById(R.id.swiperefresh);
        fab = (FloatingActionButton)findViewById(R.id.fab);

        final SearchView simpleSearchView = (SearchView) findViewById(R.id.searchView);

        simpleSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(query.length()>0){
                    Intent i=new Intent(HomeActivity.this, SearchActivity.class);
                    i.putExtra("name",""+query);
                    startActivity(i);
                    overridePendingTransition(R.anim.zoom_forward_in,R.anim.zoom_forward_out);
                }else{
                    Toast.makeText(HomeActivity.this,"Write for searching", Toast.LENGTH_LONG).show();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });


        favorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(HomeActivity.this, FavoritesActivity.class);
                startActivity(i);
            }
        });

        floatingButton();

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cursorGenres=accesData.getAllGenres();
                constructorView(cursorGenres);
                swiperefresh.setRefreshing(false);
                floatingButton();
            }
        });
        cursorGenres=accesData.getAllGenres();
        constructorView(cursorGenres);
    }

    public void floatingButton(){
        Cursor cursorAddMy=accesData.getData("AddMy");
        if(cursorAddMy.getCount()>0){
            fab.setVisibility(View.VISIBLE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Msj();
                }
            });
        }
    }

    public void Msj() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.mipmap.icono);
        builder.setTitle(getResources().getString(R.string.delete));
        builder.setMessage(getResources().getString(R.string.deseadelete));
        builder.setPositiveButton(getResources().getString(R.string.si), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                accesData.deleteDatos("AddMy");
                cursorGenres=accesData.getAllGenres();
                constructorView(cursorGenres);
                fab.setVisibility(View.GONE);
                arg0.dismiss();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    //creating dinamyc view with recyclerviews
    public void constructorView(Cursor cursor){
        linearMain.removeAllViews();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //convert from dp to pixels for dynamics margins
        DisplayMetrics metrics = HomeActivity.this.getResources().getDisplayMetrics();
        float dp = 10f;
        float fpixels = metrics.density * dp;
        int pixels = (int) (fpixels + 0.5f);

        params.leftMargin=pixels;
        params.rightMargin=pixels;
        params.topMargin=pixels;

        //my list
        Cursor cursor3=accesData.getData("AddMy");
        if(cursor3.getCount()!=0) {
            TextView txtTitleAdd=new TextView(this);
            RecyclerView recyclerAdd=new RecyclerView(this);
            txtTitleAdd.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0));
            txtTitleAdd.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0));
            txtTitleAdd.setText("Add");
            txtTitleAdd.setLayoutParams(params);
            txtTitleAdd.setTextSize(16f);

            animeHorizontalAdapter = new AnimeHorizontalAdapter(cursor3, HomeActivity.this, recyclerAdd);
            LinearLayoutManager layoutManager3 = new LinearLayoutManager(HomeActivity.this, LinearLayoutManager.HORIZONTAL, false);
            recyclerAdd.setLayoutManager(layoutManager3);
            recyclerAdd.setItemAnimator(new DefaultItemAnimator());
            recyclerAdd.setAdapter(animeHorizontalAdapter);

            linearMain.addView(txtTitleAdd);
            linearMain.addView(recyclerAdd);
        }

        //trending
        TextView txtTitleTrending=new TextView(this);
        RecyclerView recyclerTrendin=new RecyclerView(this);
        Cursor cursor1=accesData.getData("trending");

        txtTitleTrending.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT,0));
        recyclerTrendin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT,0));
        txtTitleTrending.setText("Trending");
        txtTitleTrending.setLayoutParams(params);
        txtTitleTrending.setTextSize(16f);

        animeHorizontalAdapter=new AnimeHorizontalAdapter(cursor1,HomeActivity.this,recyclerTrendin);
        LinearLayoutManager layoutManager0 = new LinearLayoutManager(HomeActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerTrendin.setLayoutManager(layoutManager0);
        recyclerTrendin.setItemAnimator(new DefaultItemAnimator());
        recyclerTrendin.setAdapter(animeHorizontalAdapter);

        linearMain.addView(txtTitleTrending);
        linearMain.addView(recyclerTrendin);

        //genres
        txtGenres = new TextView[cursor.getCount()];
        recyclerViews = new RecyclerView[cursor.getCount()];

        for(int i=0; i<cursor.getCount(); i++){
            cursor.moveToPosition(i);

            txtGenres[i] = new TextView(this);
            recyclerViews[i] =new RecyclerView(this);

            txtGenres[i].setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT,0));
            recyclerViews[i].setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT,0));
            txtGenres[i].setText(""+cursor.getString(2));
            txtGenres[i].setLayoutParams(params);
            txtGenres[i].setTextSize(16f);

            Cursor cursor2 =accesData.getData(""+cursor.getString(2));
            animeHorizontalAdapter=new AnimeHorizontalAdapter(cursor2,HomeActivity.this,recyclerViews[i]);
            LinearLayoutManager layoutManager = new LinearLayoutManager(HomeActivity.this, LinearLayoutManager.HORIZONTAL, false);
            recyclerViews[i].setLayoutManager(layoutManager);
            recyclerViews[i].setItemAnimator(new DefaultItemAnimator());
            recyclerViews[i].setAdapter(animeHorizontalAdapter);

            linearMain.addView(txtGenres[i]);
            linearMain.addView(recyclerViews[i]);

        }
    }

}
