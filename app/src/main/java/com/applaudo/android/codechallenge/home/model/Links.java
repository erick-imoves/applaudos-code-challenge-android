package com.applaudo.android.codechallenge.home.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by erick on 14/07/2018.
 */

public class Links {
    @SerializedName("self")
    String self;
    @SerializedName("related")
    String related;

    public Links(String self, String related) {
        this.self = self;
        this.related = related;
    }

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getRelated() {
        return related;
    }

    public void setRelated(String related) {
        this.related = related;
    }
}
