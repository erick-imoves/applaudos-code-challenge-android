package com.applaudo.android.codechallenge.home.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.applaudo.android.codechallenge.R;
import com.applaudo.android.codechallenge.home.adapter.SearchAdapter;
import com.applaudo.android.codechallenge.home.model.Anime;
import com.applaudo.android.codechallenge.home.model.Datum;
import com.applaudo.android.codechallenge.rest.ApiClient;
import com.applaudo.android.codechallenge.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {
    ApiInterface apiService;
    ProgressDialog mProgressDialog;
    RecyclerView recyclerSearch;
    List<Datum> listDatos;
    SearchAdapter searchAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(R.string.searchTitle);

        apiService= ApiClient.getClient().create(ApiInterface.class);

        recyclerSearch=(RecyclerView)findViewById(R.id.recyclerSearch);

        String name=getIntent().getExtras().getString("name");
        getData(name);
    }

    public void getData(String name){
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(getResources().getString(R.string.cargando));
        mProgressDialog.show();

        Call<Anime> call = apiService.dateFilter(name);

        call.enqueue(new Callback<Anime>() {
            @Override
            public void onResponse(Call<Anime> call, Response<Anime> response) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                try {
                    listDatos = response.body().getData();
                    searchAdapter = new SearchAdapter(listDatos, SearchActivity.this, recyclerSearch);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(SearchActivity.this, LinearLayoutManager.VERTICAL, false);
                    recyclerSearch.setLayoutManager(layoutManager);
                    recyclerSearch.setItemAnimator(new DefaultItemAnimator());
                    recyclerSearch.setAdapter(searchAdapter);
                }catch (Exception e){
                    Log.i("example", "Error, body: " + e.toString());
                }
            }

            @Override
            public void onFailure(Call<Anime> call, Throwable t) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                Log.i("Error", "not conecction with server: " + t.toString());
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
