package com.applaudo.android.codechallenge.db;

import android.database.sqlite.SQLiteDatabase;

import java.util.Map;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.AbstractDaoSession;
import de.greenrobot.dao.identityscope.IdentityScopeType;
import de.greenrobot.dao.internal.DaoConfig;
/**
 * Created by erick on 16/07/2018.
 */

public class DaoSession extends AbstractDaoSession {

    private final DaoConfig dbDatosDaoConfig;
    private final DaoConfig dbGeneralGenresConfig;

    private final Db_datosDao dbDatosDao;
    private final Db_generalGenresDao dbGeneralGenresDao;

    public DaoSession(SQLiteDatabase db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        dbDatosDaoConfig = daoConfigMap.get(Db_datosDao.class).clone();
        dbDatosDaoConfig.initIdentityScope(type);

        dbGeneralGenresConfig = daoConfigMap.get(Db_generalGenresDao.class).clone();
        dbGeneralGenresConfig.initIdentityScope(type);

        dbDatosDao = new Db_datosDao(dbDatosDaoConfig, this);
        dbGeneralGenresDao = new Db_generalGenresDao(dbGeneralGenresConfig,this);

        registerDao(Db_datos.class, dbDatosDao);
        registerDao(Db_generalGenres.class,dbGeneralGenresDao);
    }

    public void clear() {
        dbDatosDaoConfig.getIdentityScope().clear();
        dbGeneralGenresConfig.getIdentityScope().clear();
    }

    public Db_datosDao getdbDatosDao() {
        return dbDatosDao;
    }

    public Db_generalGenresDao getDbGeneralGenresDao() {
        return dbGeneralGenresDao;
    }

}