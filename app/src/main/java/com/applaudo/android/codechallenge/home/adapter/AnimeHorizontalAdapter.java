package com.applaudo.android.codechallenge.home.adapter;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.applaudo.android.codechallenge.R;
import com.applaudo.android.codechallenge.detail.activity.DetailActivity;
import com.bumptech.glide.Glide;

/**
 * Created by erick on 14/07/2018.
 */

public class AnimeHorizontalAdapter extends RecyclerView.Adapter<AnimeHorizontalAdapter.MyViewHolder> {
    Cursor cursor;
    Activity activity;
    RecyclerView recycler_view;

    public AnimeHorizontalAdapter(Cursor cursor, Activity activity, RecyclerView recycler_view) {
        this.cursor = cursor;
        this.activity = activity;
        this.recycler_view = recycler_view;
    }

    @Override
    public AnimeHorizontalAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_horizontal_list_anime, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AnimeHorizontalAdapter.MyViewHolder holder, final int position) {
        cursor.moveToPosition(position);
        holder.txtTitulo.setText(cursor.getString(11));
        Glide.with(activity)
                .load(cursor.getString(12))
                .into(holder.imgPoster);
        holder.imgPoster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cursor.moveToPosition(position);
                Intent i=new Intent(activity, DetailActivity.class);
                i.putExtra("id",""+cursor.getString(0));
                activity.startActivity(i);
                activity.overridePendingTransition(R.anim.zoom_back_in,R.anim.zoom_back_out);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cursor.getCount();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitulo;
        ImageView imgPoster;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtTitulo=itemView.findViewById(R.id.txtTitulo);
            imgPoster=itemView.findViewById(R.id.imgPoster);
        }
    }
}
