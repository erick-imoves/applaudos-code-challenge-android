package com.applaudo.android.codechallenge.detail.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by erick on 15/07/2018.
 */

public class DataEpisodes {
    @SerializedName("id")
    String id;
    @SerializedName("type")
    String type;
    @SerializedName("attributes")
    AttributesEpisode attributes;

    public DataEpisodes(String id, String type, AttributesEpisode attributes) {
        this.id = id;
        this.type = type;
        this.attributes = attributes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public AttributesEpisode getAttributes() {
        return attributes;
    }

    public void setAttributes(AttributesEpisode attributes) {
        this.attributes = attributes;
    }
}
