package com.applaudo.android.codechallenge.home.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by erick on 14/07/2018.
 */

public class Relationships {
    @SerializedName("episodes")
    Episodes episodes;
    @SerializedName("animeCharacters")
    AnimeCharacters animeCharacters;

    public Relationships(Episodes episodes, AnimeCharacters animeCharacters) {
        this.episodes = episodes;
        this.animeCharacters = animeCharacters;
    }

    public Episodes getEpisodes() {
        return episodes;
    }

    public void setEpisodes(Episodes episodes) {
        this.episodes = episodes;
    }

    public AnimeCharacters getAnimeCharacters() {
        return animeCharacters;
    }

    public void setAnimeCharacters(AnimeCharacters animeCharacters) {
        this.animeCharacters = animeCharacters;
    }
}
