package com.applaudo.android.codechallenge.home.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by erick on 15/07/2018.
 */

public class Detail {
    @SerializedName("data")
    Datum data;

    public Detail(Datum data) {
        this.data = data;
    }

    public Datum getData() {
        return data;
    }

    public void setData(Datum data) {
        this.data = data;
    }
}
