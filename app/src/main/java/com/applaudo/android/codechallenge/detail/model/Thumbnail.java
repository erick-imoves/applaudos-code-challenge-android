package com.applaudo.android.codechallenge.detail.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by erick on 15/07/2018.
 */

public class Thumbnail {
    @SerializedName("original")
    String original;

    public Thumbnail(String original) {
        this.original = original;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }
}
