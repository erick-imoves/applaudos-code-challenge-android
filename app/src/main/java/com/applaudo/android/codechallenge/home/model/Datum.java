package com.applaudo.android.codechallenge.home.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by erick on 14/07/2018.
 */

public class Datum {
    @SerializedName("id")
    String id;
    @SerializedName("type")
    String type;
    @SerializedName("attributes")
    Attributes attributes;
    @SerializedName("relationships")
    Relationships relationships;

    public Datum(String id, String type, Attributes attributes, Relationships relationships) {
        this.id = id;
        this.type = type;
        this.attributes = attributes;
        this.relationships = relationships;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public Relationships getRelationships() {
        return relationships;
    }

    public void setRelationships(Relationships relationships) {
        this.relationships = relationships;
    }
}
