package com.applaudo.android.greendao_generator;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class myMain {
    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1, "com.applaudo.android.codechallenge.db");

        Entity datos = schema.addEntity("Db_datos");
        datos.addIdProperty();
        datos.addStringProperty("idDatos");
        datos.addStringProperty("type");
        datos.addStringProperty("genre");




        new DaoGenerator().generateAll(schema, "../app/src/myMain/java");

    }
}
